package com.diogofernando.crud.exception;


import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ExceptionResponse implements Serializable {

	
	private static final long serialVersionUID = -4641272380203014571L;

	private Date timestamp;	
	private String message;	
	private String details;
	
	
	
}		

