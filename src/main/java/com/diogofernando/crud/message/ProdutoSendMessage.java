package com.diogofernando.crud.message;


import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.diogofernando.crud.data.vo.ProdutoVO;
import com.diogofernando.crud.repository.ProdutoRepository;

import lombok.RequiredArgsConstructor;


@Component
@RequiredArgsConstructor
public class ProdutoSendMessage {
	
	
	@Value("${crud.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${crud.rabbitmq.routingkey}")
	private String routingkey;
	
	
    private RabbitTemplate rabbitTemplate;
    
    
    @Autowired
    public ProdutoSendMessage(RabbitTemplate rabbitTemplate) {
		
		this.rabbitTemplate = rabbitTemplate;
	}



	
	public void sendMessage(ProdutoVO produtoVO) {
		rabbitTemplate.convertAndSend(exchange,routingkey,produtoVO);
		
	}


}
